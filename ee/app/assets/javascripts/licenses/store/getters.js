export const hasLicenses = state => state.licenses.length > 0;
